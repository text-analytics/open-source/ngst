import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PolymorphicContainerDirective } from './directives/polymorphic-container.directive';
import { StopPropogationDirective } from './directives/stop-propogation.directive';
import { DynamicCellComponent } from './dynamic-cell/dynamic-cell.component';
import { FilterCellComponent } from './filter-cell/filter-cell';
import { FilterCellWrapperComponent } from './filter-cell/filter-cell-wrapper.component';
import { IntFilterComponent } from './filter-cell/int-filter/int-filter.component';
import { StringFilterComponent } from './filter-cell/string-filter/string-filter.component';
import { BooleanInputComponent } from './inputs/boolean-input/boolean-input.component';
import { InputContainerComponent } from './inputs/input-container/input-container.component';
import { RawInputComponent } from './inputs/raw-input/raw-input.component';
import { TextAreaInputComponent } from './inputs/raw-input/text-area-input.component';
import { SelectionInputComponent } from './inputs/selection-input/selection-input.component';
import { NewRowDialogComponent } from './new-row-dialog/new-row-dialog.component';
import { TableComponent } from './table/table.component';


@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatIconModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatSelectModule,
    MatTooltipModule,
    MatCheckboxModule
  ],
  declarations: [
    TableComponent,
    PolymorphicContainerDirective,
    RawInputComponent,
    TextAreaInputComponent,
    SelectionInputComponent,
    BooleanInputComponent,
    InputContainerComponent,
    NewRowDialogComponent,
    DynamicCellComponent,
    StopPropogationDirective,
    FilterCellWrapperComponent,
    IntFilterComponent,
    FilterCellComponent,
    StringFilterComponent
  ],
  exports: [
    TableComponent,
    RawInputComponent,
    TextAreaInputComponent,
    SelectionInputComponent,
    BooleanInputComponent,
  ],
  entryComponents: [
    FilterCellWrapperComponent,
    RawInputComponent,
    TextAreaInputComponent,
    SelectionInputComponent,
    BooleanInputComponent,
    NewRowDialogComponent
  ]
})
export class NgstModule {
}
